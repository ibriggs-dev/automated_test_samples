import unittest
from util.page import Page, SignUpPage
from selenium import webdriver
from selenium.webdriver.support.select import Select

class FacebookRegistrationTest(unittest.TestCase):
    def setUp(self):
        # create a new Firefox session
        self.driver = webdriver.Firefox()
        self.driver.get("https://test-url")
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def test_facebook_registration(self):
        sign_up_page = SignUpPage(self.driver)
        assert sign_up_page.check_signup_wrap().value_of_css_property('display') == 'block', "Signup Wrap not displayed"
        primary_window = sign_up_page.driver.window_handles[0]
        sign_up_page.submit_facebook()
        fb_window = sign_up_page.driver.window_handles[1]
        sign_up_page.driver.switch_to.window(fb_window)
        sign_up_page.enter_facebook_details()
        sign_up_page.driver.switch_to.window(primary_window)
        Select(sign_up_page.driver.find_element_by_id('user_secret_question_id')).select_by_value('1')
        sign_up_page.send_input(sign_up_page.driver.find_element_by_id('user_secret_answer'), 'test')
        sign_up_page.driver.find_element_by_id('user_secret_answer').submit()

        
    def tearDown(self):
        # close the browser window
        self.driver.quit()
 
if __name__ == '__main__':
    unittest.main(verbosity=2)
