from util.locators import SignUpLocators, EmpLocators
from selenium.webdriver.support.ui import WebDriverWait

class Page():
    "Page class that all page models can inherit from"
    def __init__(self, driver):
        "Constructor"
        self.driver = driver
    
    def send_input(self, element, user_input):
        "Send keys to field"
        element.send_keys(user_input)

    def page_wait(self):
        "Wait for jquery to finish"
        wait = WebDriverWait(self.driver, 10)
        wait.until(lambda driver: self.driver.execute_script("return jQuery.active == 0"))

class SignUpPage(Page):
    "Standard EME Sign Up page object"
    def check_signup_wrap(self):
        return self.driver.find_element(*SignUpLocators.signup_wrap)

    def check_verify_wrap(self):
        return self.driver.find_element(*SignUpLocators.verify_wrap)

    def set_email(self, email):
        "Set email"
        user_email_field = self.driver.find_element(*SignUpLocators.user_email_field)
        user_email_field.send_keys(email)

    def set_password(self, password):
        "Set password"
        user_password_field = self.driver.find_element(*SignUpLocators.user_password_field)
        user_password_field.send_keys(password)

    def submit_sign_up(self):
        "Submit registration"
        submit_button = self.driver.find_element(*SignUpLocators.submit_button)
        submit_button.click()

    def submit_facebook(self):
        facebook_register = self.driver.find_element(*SignUpLocators.facebook_register)
        facebook_register.click()

    def enter_facebook_details(self):
        fb_email = self.driver.find_element_by_id('email')
        fb_email.send_keys('test-email@test.com')
        fb_pw = self.driver.find_element_by_id('pass')
        fb_pw.send_keys('test-password')
        fb_pw.submit()
        self.driver.find_element_by_name('__CONFIRM__').click()

class EmpPage(Page):
    "EMP Store page object"

    def is_title_matches(self):
        return "En Masse Points (EMP) | TERA" in self.driver.title

    def sign_in(self):
        element = self.driver.find_element(*EmpLocators.sign_in_button)
        element.click()
        self.page_wait()
        self.driver.switch_to.frame(self.driver.find_element(*EmpLocators.page_iframe))
        #enter email
        email_field = self.driver.find_element(*EmpLocators.user_email_field)
        password_field = self.driver.find_element(*EmpLocators.user_password_field)
        #enter password
        email_field.send_keys("test@test.com")
        password_field.send_keys("test-password")
        password_field.submit()
