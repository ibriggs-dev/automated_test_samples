from selenium.webdriver.common.by import By

class SignUpLocators():
    "Elements on sign-up page"
    user_email_field = (By.ID, 'user_email')
    user_password_field = (By.ID, 'user_password')
    submit_button = (By.ID, 'btn-create-account')
    facebook_register = (By.XPATH, '//*[@id="app-account"]/section/div/div/div[1]/a')
    verify_wrap = (By.ID, 'verify-wrap')
    signup_wrap = (By.CLASS_NAME, 'signup-wrap')
    resend_verification = (By.XPATH, '//*[@id="verify-wrap"]/p[2]/a')
    footer_container = (By.XPATH, '//*[@id="eme-footer"]/div/div/p')


class EmpLocators():
    "Elements on EMP page"
    sign_in_button = (By.XPATH, '/html/body/div[4]/div/div[3]/a')
    page_iframe = (By.XPATH, '//*[@id="login-box"]/iframe')
    user_email_field = (By.XPATH, '//*[@id="user_email"]')
    user_password_field = (By.XPATH, '//*[@id="user_password"]')
    log_in_button = (By.CLASS_NAME, 'submit-link login-submit-link btn-login')
