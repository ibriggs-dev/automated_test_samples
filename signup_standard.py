import unittest
from util.page import SignUpPage
from util.randomemail import generate_email
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class ValidRegistrationTest(unittest.TestCase):
    def setUp(self):
        # create a new Firefox session
        self.driver = webdriver.Firefox()
        self.driver.get("https://test-url")
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def test_valid_registration(self):
        sign_up_page = SignUpPage(self.driver)
        assert sign_up_page.check_signup_wrap().value_of_css_property('display') == 'block', "Signup Wrap not displayed"
        sign_up_page.set_email(generate_email())
        sign_up_page.page_wait()
        sign_up_page.set_password("P@ssword")
        sign_up_page.page_wait()
        sign_up_page.submit_sign_up()
        WebDriverWait(self.driver, 30).until(EC.visibility_of(sign_up_page.check_verify_wrap()))
        assert sign_up_page.check_verify_wrap().value_of_css_property('display') == 'block', "Verify Wrap not displayed"
        assert sign_up_page.check_signup_wrap().value_of_css_property('display') == 'none', "Verify Wrap not hidden"

    def tearDown(self):
        # close the browser window
        self.driver.quit()

if __name__ == '__main__':
    unittest.main(verbosity=2)
