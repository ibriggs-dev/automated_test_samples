import sys
import unittest
import signup_standard
import signup_facebook
 
 
class Test_Suite(unittest.TestCase):
   
    def test_signup(self):
         
        # suite of TestCases
        self.suite = unittest.TestSuite()
        self.suite.addTests([            
            unittest.defaultTestLoader.loadTestsFromTestCase(signup_standard.ValidRegistrationTest),
            unittest.defaultTestLoader.loadTestsFromTestCase(signup_facebook.FacebookRegistrationTest),
                                 
            ])
        runner = unittest.TextTestRunner()
        runner.run (self.suite)
 
import unittest
if __name__ == "__main__":
    unittest.main(verbosity=2)